#!/bin/env sh

########
# WD14 #
########

if findmnt -M /btr_pool/WDELEMENTS14 > /dev/null; then
	echo umount WD14
	sudo umount /btr_pool/WDELEMENTS14 &&
	sudo cryptsetup close /dev/mapper/wdelements14 &&
	echo WD14 can be removed
fi

########
# WD16 #
########

if findmnt -M /btr_pool/WDELEMENTS16 > /dev/null; then
	echo umount WD16
	sudo umount /btr_pool/WDELEMENTS16 &&
	sudo cryptsetup close /dev/mapper/wdelements16 &&
	echo WD16 can be removed
fi
