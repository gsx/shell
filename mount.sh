#!/bin/env sh

########
# WD14 #
########

if ! findmnt -M /btr_pool/WDELEMENTS14 > /dev/null; then
	[ -h /dev/disk/by-uuid/faa14592-2a40-417a-b61f-44e9310f47a6 ] && echo mount WD USB 14TB &&
	sudo cryptsetup open /dev/disk/by-uuid/faa14592-2a40-417a-b61f-44e9310f47a6 wdelements14 --key-file /etc/cryptwdelements14.key && sleep 5 &&
	sudo mount -t btrfs -o relatime,rw,compress=lzo,space_cache=v2 /dev/mapper/wdelements14 /btr_pool/WDELEMENTS14
fi

########
# WD16 #
########

if ! findmnt -M /btr_pool/WDELEMENTS16 > /dev/null; then
	[ -h  /dev/disk/by-uuid/a294933a-6eaf-42b0-9229-7ffb2dac416b ] && echo mount WD USB 16TB &&
	sudo cryptsetup open /dev/disk/by-uuid/a294933a-6eaf-42b0-9229-7ffb2dac416b wdelements16 --key-file /etc/cryptwdelements16.key && sleep 5 &&
	sudo mount -t btrfs -o relatime,rw,compress=lzo,space_cache=v2 /dev/mapper/wdelements16 /btr_pool/WDELEMENTS16
fi

