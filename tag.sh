#!/bin/zsh

echo "$1"

ARTIST=$(mediainfo "$1" | grep "^Performer " | awk -F ': ' '{print $2}')
ALBUMARTIST=$(mediainfo "$1" | grep "^Album/Performer " | awk -F ': ' '{print $2}')
ALBUM=$(mediainfo "$1" | grep "^Album " | awk -F ': ' '{print $2}')
TRACK=$(mediainfo "$1" | grep "^Track name " | awk -F ': ' '{print $2}')
TRACKNR=$(mediainfo "$1" | grep "^Track name/Position " | awk -F ': ' '{print $2}')
RELEASEDATE=$(mediainfo "$1" | grep "^Recorded date " | awk -F ': ' '{print $2}')
COMMENT=$(mediainfo "$1" | grep "^Comment " | awk -F ': ' '{print $2}')
COVER=$(find "$(dirname "$1")" -iname 'cover.*')
MP4FILE="${1%.ogg}.m4a"

# echo "Artist:      $ARTIST"
# echo "AlbumArtist: $ALBUMARTIST"
# echo "Album:       $ALBUM"
# echo "Track:       $TRACK"
# echo "TrackNR:     $TRACKNR"
# echo "Date:        $RELEASEDATE"
# echo "Coment:      $COMMENT"
# echo "Cover:       $COVER"
# echo "mp4 file:    $MP4FILE"

# Tag with Coveratwork
atomicparsley $MP4FILE \
	--artist "$ARTIST" \
	--albumArtist "$ALBUMARTIST" \
	--album "$ALBUM" \
	--title "$TRACK" \
	--tracknum "$TRACKNR" \
	--year "$RELEASEDATE" \
	--comment "$COMMENT" \
	--artwork "$COVER"

# Tag without Coveratwork
# atomicparsley $MP4FILE \
	# --artist "$ARTIST" \
	# --albumArtist "$ALBUMARTIST" \
	# --album "$ALBUM" \
	# --title "$TRACK" \
	# --tracknum "$TRACKNR" \
	# --year "$RELEASEDATE" \
	# --comment "$COMMENT"
