#!/bin/env sh

if ! [ -d /tmp/mail-backup ]; then
	mkdir /tmp/mail-backup
fi

if ! findmnt -M /tmp/mail-backup > /dev/null; then
	sshfs gsx: /tmp/mail-backup
fi

restic --repo /home/gsx/media/14/Backup/Mail/restic \
--password-file /etc/cryptresticmail.key \
--exclude="dovecot*" \
backup \
/tmp/mail-backup/Maildir \
/tmp/mail-backup/users

export BORG_PASSCOMMAND='cat /etc/cryptborgmail.key'
borg create \
--progress \
--stats \
--exclude="*/dovecot*" \
/home/gsx/media/14/Backup/Mail/borg::$(date -Iminutes) \
/tmp/mail-backup/Maildir \
/tmp/mail-backup/users

fusermount -u /tmp/mail-backup &&
rmdir /tmp/mail-backup
