#!/bin/sh

sudo fd . /srv/minidlna -x chown minidlna:minidlna
sudo fd -t d . /srv/minidlna -x chmod 775
sudo fd -t f . /srv/minidlna -x chmod 664
