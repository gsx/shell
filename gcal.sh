#!/bin/sh
gcal -K --iso-week-number=yes -s1 -qDE_BY -H '\e[32m:\e[0m:\e[34m:\e[0m' --force-highlighting .+ | rg -v -e '^ *$|^ *\d{4} *$'
