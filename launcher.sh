#!/bin/zsh

export FZF_DEFAULT_OPTS=" \
	--color=fg:#f8f8f2,bg:#181a26,hl:#bd93f9 \
	--color=fg+:#f8f8f2,bg+:#44475a,hl+:#00fa3f \
	--color=info:#ffb86c,prompt:#50fa7b,pointer:#ff79c6 \
	--color=marker:#ff79c6,spinner:#ffb86c,header:#6272a4"

echo -ne "\x1b]0;LAUNCHER\x1b\\"

BASENAME=$(/usr/bin/ls "$HOME/.local/launcher" | sort -u | fzf)
FULLPATH="$HOME/.local/launcher/""$BASENAME"

swaymsg exec -- $FULLPATH
